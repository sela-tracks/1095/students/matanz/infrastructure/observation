# Observation

**Introduction**

Prometheus and Grafana work together to render monitoring. Prometheus takes care of data fetching as a data source and feeds that data into Grafana, which can be used to visualize data with attractive dashboards.

**Requirements:**

* Installed kubectl command-line tool
* Connected to a Kubernetes cluster - Have a kubeconfig file (default location is /kube/config)

**Installing Prometheus and Grafana**

Use the following commands:
```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
```
Next, we need to install Prometheus using the following command:
```
helm install prometheus prometheus-community/prometheus
```
It will install all the required components of the Prometheus system with a single command. Without Helm Charts we would have to write the manifest file ourselves.

We can expose the prometheus-server service to the internet using nodeport but the GUI provided by prometheus is not as good as the one provided by Grafana. We can use the following command for the same:
```
kubectl expose service prometheus-server --type=NodePort --target-port=9090 -- name=prometheus-server-ext
```
We can install Grafana using the following command:
```
helm install grafana grafana/grafana
```
We can expose Grafana service to the external world using the following commands:
```
kubectl expose service grafana --type=NodePort --target-port=3000 --name=grafana-ext
```
We can find the username and password required to log in into Grafana using the following commands. It will show the values in encrypted format, which we can decode using OpenSSL and base 64 formats.
Use this commands:
```
kubectl get secret --namespace default grafana -o yaml
echo “password_value” | openssl base64 -d ; echo
echo “username_value” | openssl base64 -d ; echo
```

**Add prometheus data source into Grafana dashboard**

1. Login into the Grafana dashboard using the admin user and password generated in the previous step.
In the add data source section, provide the service URL generated for prometheus-server-ext after installation and save it.

2. Next, we need to create a new dashboard. We can either create a dashboard from scratch or import the dashboards available for Prometheus on grafana.com. For the simplicity of this article, we are using a dashboard available on grafana.com numbered “3662”. Add the dashboard number and hit load.

3. On the next screen add the data source for this dashboard which is Prometheus in our case and hit import.

We will have a beautiful dashboard ready for us which we can modify according to our requirements.


